from pytest import raises
from search_treasure import Search

search = Search()

def test_search_treasure_not_found():
    with raises(AssertionError):
        a = search.executing_search()
        assert a == (-1, -1)

def test_create_map():
    with raises(AssertionError):
        _map = search.create_map(2)
        assert _map == [(0,0), (1, 1), (1, 0), (0, 1)]